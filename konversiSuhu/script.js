function suhu() {
  var numb = document.getElementById("numb").value;
  var konversi = document.getElementById("konversi").value;
  var hasil;
  if (konversi == "CF") {
    hasil = (9 / 5) * parseInt(numb) + 32;
  } else if (konversi == "CR") {
    hasil = (4 / 5) * parseInt(numb);
  } else if (konversi == "CK") {
    hasil = parseInt(numb) + 273.15;
  } else if (konversi == "FC") {
    hasil = (parseInt(numb) - 32) / (9 / 5);
  } else if (konversi == "FR") {
    hasil = (parseInt(numb) - 32) / (9 / 4);
  } else if (konversi == "FK") {
    hasil = (parseInt(numb) + 459.67) / (9 / 5);
  } else if (konversi == "RC") {
    hasil = parseInt(numb) / (4 / 5);
  } else if (konversi == "RF") {
    hasil = (9 / 4) * parseInt(numb) + 32;
  } else if (konversi == "RK") {
    hasil = parseInt(numb) / (4 / 5) + 273.15;
  } else if (konversi == "KC") {
    hasil = parseInt(numb) - 273.15;
  } else if (konversi == "KF") {
    hasil = parseInt(numb) * (9 / 5) - 459.67;
  } else if (konversi == "KR") {
    hasil = (parseInt(numb) - 273.15) * (9 / 5);
  } else {
    hasil = "tidak ada";
  }

  alert("hasil dari konversi adalah : " + hasil);
}
